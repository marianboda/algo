import { play } from './patmat'
import fs from 'fs'
import readline from 'readline'
import stream from 'stream'

const args = process.argv.slice(2)

const file = (args.length > 0) ? args[0] : null

if (file === null) {
  const a = [1, 2, 3, 10, 17, 47, 999999999]

  a.forEach(i => {
    console.log(play(i))
  })
} else {
  const instream = fs.createReadStream(file);
  const outstream = new stream;
  const rl = readline.createInterface(instream, outstream);

  let results = []

  rl.on('line', function(line) {
    console.log(play(Number(line)))
  });

  rl.on('close', function() {
    // console.log(cnt)
  });
}
