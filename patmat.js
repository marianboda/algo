import process from 'process'

export const popcount = (x) => {
  x -= x >> 1 & 0x55555555
  x = (x & 0x33333333) + (x >> 2 & 0x33333333)
  x = x + (x >> 4) & 0x0f0f0f0f
  x += x >> 8
  x += x >> 16

  return x & 0x7f
}

export const getPowersUpTo = (n) => {
  const a = []
  for (let i = 1; i < n; i = i << 1) {
    a.push(i)
  }
  return a
}

const getPlayer = (i) => ['PAT','MAT'][i % 2]

export const play = (d, iter = 0) => {
  // if (iter == 0) {
  //   console.log(`N: ${d}`, getPlayer(iter))
  // }
  const powers = getPowersUpTo(d)
  const validMovements = powers.filter(i => {
    return popcount(d - i) == popcount(d)
  })

  if (validMovements.length === 0) {
    return ['PAT','MAT'][(iter+1) % 2]
  }

  return play(d - validMovements[0], iter + 1)
}
