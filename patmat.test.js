import { popcount, getPowersUpTo, playAll, play } from './patmat'

test ('getPowersUpTo should work', () => {
  const powers12 = getPowersUpTo(12)
  const powers1200 = getPowersUpTo(1200)
  expect(powers12).toEqual([1,2,4,8])
  expect(powers1200).toEqual([1,2,4,8,16,32,64,128, 256, 512, 1024])
})

test('popcount should work', () => {
  expect(popcount(255)).toEqual(8)
  expect(popcount(256)).toEqual(1)
  expect(popcount(0)).toEqual(0)
  expect(popcount(1)).toEqual(1)
  expect(popcount(1024)).toEqual(1)
})

test('results should match', () => {
  expect(play(1)).toEqual('MAT')
  expect(play(2)).toEqual('PAT')
  expect(play(3)).toEqual('MAT')
  expect(play(10)).toEqual('PAT')
  expect(play(17)).toEqual('PAT')
  expect(play(47)).toEqual('PAT')
  expect(play(999999999)).toEqual('MAT')
})

// test('result should match snapshot', () => {
//   expect(playAll()).toMatchSnapshot()
// })
